<?php

namespace Vector5\DB\TreeWalker\Laravel;

use Vector5\DB\TreeWalker\Postgres\PDOTreeWalker;
use Illuminate\Support\ServiceProvider;

class TreeWalkerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config.php' => config_path('dbtreewalker.php')
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config.php', 'dbtreewalker');

        $this->app->singleton('dbtreewalker', function ($app) {
            return new TreeWalkerManager($app);
        });
    }
}