<?php

namespace Vector5\DB\TreeWalker\Laravel\Facades;

use Illuminate\Support\Facades\Facade;

class TreeWalker extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'dbtreewalker';
    }
}