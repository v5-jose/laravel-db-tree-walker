<?php

namespace Vector5\DB\TreeWalker\Laravel;

use Vector5\DB\TreeWalker\TreeWalker;
use Vector5\DB\TreeWalker\DoctrineTreeWalker;
use Illuminate\Database\Connection;

class DatabaseConnectionTreeWalker implements TreeWalker
{
    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     * @param \Illuminate\Database\Connection
     */
    public function __construct(Connection $connection = null)
    {
        $this->connection = $connection;
    }

    /**
     * Load the schema.
     * 
     * @param string $database
     * @param array $config (optional)
     * @return \Vector5\DB\TreeWalker\Schema 
     */
    public function load($database, array $config = [])
    {
        // Use DBAL's schema manager if available
        if ($this->connection->isDoctrineAvailable()) {
            $doctrineTreeWalker = new DoctrineTreeWalker($this->connection->getDoctrineConnection());

            return $doctrineTreeWalker->load($database, $config);
        }
    }

    /**
     * Set the connection used for the treewalker.
     * 
     * @param \Illuminate\Database\Connection $connection
     * @return self
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
        
        return $this;
    }

    /**
     * Get the database connection used by the treewalker
     * 
     * @return \Illuminate\Database\Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }
}