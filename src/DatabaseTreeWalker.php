<?php

namespace Vector5\DB\TreeWalker\Laravel;

use Vector5\DB\TreeWalker\TreeWalker;
use Illuminate\Database\DatabaseManager;

class DatabaseTreeWalker implements TreeWalker
{
    /**
     * @var \Illuminate\Database\DatabaseManager
     */
    protected $dbManager;

    /**
     * @param \Illuminate\Database\DatabaseManager $manager
     */
    public function __construct(DatabaseManager $manager)
    {
        $this->dbManager = $manager;
    }

    /**
     * Load the schema.
     * 
     * @param string $database
     * @param array $config (optional)
     * @return \Vector5\DB\TreeWalker\Schema 
     */
    public function load($database, array $config = [])
    {
        $connection = $this->dbManager->connection($database);

        return (new DatabaseConnectionTreeWalker($connection))
            ->load($database, $config);
    }
}