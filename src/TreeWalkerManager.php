<?php

namespace Vector5\DB\TreeWalker\Laravel;

use Vector5\DB\TreeWalker\DoctrineTreeWalker;
use Vector5\DB\TreeWalker\Postgres\PDOTreeWalker as PostgresPDOTreeWalker;
use Vector5\DB\TreeWalker\Postgres\PGTreeWalker;
use Vector5\DB\TreeWalker\Contracts\Factory;
use Illuminate\Database\Connection;
use Illuminate\Support\Manager;
use PDO;
use InvalidArgumentException;

class TreeWalkerManager extends Manager
{
    /**
     * Get the default driver name.
     * 
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->app['config']['dbtreewalker.default'] ?? 'db';
    }

    /**
     * Create a database treewalker driver.
     * 
     * @return \Vector5\DB\TreeWalker\Laravel\DatabaseTreeWalker
     */
    protected function createDbDriver()
    {
        return new DatabaseTreeWalker($this->app['db']);
    }

    /**
     * Create a database connection treewalker driver
     * 
     * @return \Vector5\DB\TreeWalker\Laravel\DatabaseConnectionTreeWalker
     */
    protected function createDbConnectionDriver()
    {
        return new DatabaseConnectionTreeWalker();
    }
}